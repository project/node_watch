CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Node Watch module provides a report of nodes on the system in order  
to provide alerts when that count changes based on a configurable threshold.


 * For a full description of the module, visit the project page:
   https://drupal.org/project/node_watch


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/node_watch

REQUIREMENTS
------------
This module does not require any other modules.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:


   - Administer node watch settings.


     Perform administration tasks for the node watch project.


   - Create node watch report


     Allows a user to create a node watch report.


 * Customize the menu settings in Administration » Configuration and modules »
   Development » Node Watch.
